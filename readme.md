# oEmbed : simulation des services sans oEmbed

Ce plugin permet de simuler oEmbed pour des sites qui ne fourniraient pas eux-mêmes ce standard.

Pour cela il faut ajouter la déclaration d'un masque de détection pour le site concerné, dans le pipeline d'oEmbed, en indiquant comme endpoint notre propre site :
```php
$providers['http://*.sitenulsansoembed.com/*'] = $GLOBALS['meta']['adresse_site'] . '/oembed_fake.api/sitenulsansoembed';
```

Puis il faut créer un fichier et une fonction du nom de ce nouveau service : `oembed/input/sitenulsansoembed.php`

```php
function oembed_input_sitenulsansoembed_dist($args) {
	$data = [];
	
	// Chercher les données selon ce que fournit ce site (API de son cru ou autre, à l'arrache dans le HTML, etc)
	// et remplir les données du standard oEmbed
	$data['title'] = …
	$data['author_name'] = …
	$data['html'] = …
	
	return $data;
}
```

Pour l'instant le plugin prend en charge le service de vidéo académique Canal-U. Cependant le système de temporisation de la lecture de la vidéo ne marche pas (contrairement à Youtube ou autre).
