<?php

/**
 * Plugin oEmbed
 * Licence GPL3
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_api_oembed_fake_dist() {
	$args = [
		'url' => $url = _request('url'),
		'maxheight' => _request('maxheight'),
		'maxwidth' => _request('maxwidth'),
		'format' => _request('format'),
		// support du jsonp: http://json-p.org/
		'callback_jsonp' => _request('callback_jsonp'),
	];

	// Un pipeline pour pouvoir manipuler les arguments (en ajouter des spécifiques par ex.)
	$args = pipeline('oembed_liste_arguments', $args);

	$format = ($args['format'] == 'xml' ? 'xml' : 'json');

	$md5 = md5(serialize($args));
	$oembed_cache = sous_repertoire(_DIR_CACHE, substr($md5, 0, 1)) . 'oe-' . $md5 . '.' . $format;

	// si cache oembed dispo et pas de recalcul demande, l'utiliser (perf issue)
	$res = '';
	if (
		file_exists($oembed_cache)
		and _VAR_MODE !== 'recalcul'
		and (!defined('_VAR_NOCACHE')
			or !_VAR_NOCACHE)
	) {
		lire_fichier($oembed_cache, $res);
	} else {
		// Le service doit être donné dans l'URL
		$service = preg_replace(',\W,', '', _request('arg'));
		
		// Chercher une fonction dédiée au service demandé qui doit renvoyer le tableau de data oembed
		if ($f = charger_fonction($service, 'oembed/input')) {
			$data = $f($args);

			if ($format == 'xml') {
				$output = charger_fonction('xml', 'oembed/output');
				$res = $output($data, false);
			}
			else {
				$res = json_encode($data);
			}
		}
		ecrire_fichier($oembed_cache, $res);
	}

	if (!$res) {
		include_spip('inc/headers');
		http_response_code(404);
		echo '404 Not Found';
	} else {
		$content_type = ($format == 'xml' ? 'text/xml' : 'application/json');
		header("Content-type: $content_type; charset=utf-8");
		echo $res;
	}
}
