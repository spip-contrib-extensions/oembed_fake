<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function oembed_input_canalu_dist($args) {
	include_spip('inc/distant');
	include_spip('inc/filtres');
	$data = [];
	
	// On récupère la page entière du coup…
	if ($url = recuperer_url($args['url'])) {
		$page = $url['page'];
		
		// On cherche le titre à l'arrache
		if (preg_match('|<h1[^>]*>(.*?)</h1>|i', $page, $trouve)) {
			$data['title'] = trim(supprimer_tags($trouve[1]));
		}
		
		// On cherche le JSON de Drupal
		if (preg_match('|<script[^>]*data-drupal-selector[^>]*>(.*?)</script>|i', $page, $trouve)) {
			$json = $trouve[1];
			$json = json_decode($json, true);
			
			if (!empty($json['player'])) {
				foreach ($json['player'] as $video) {
					$data['type'] = 'video';
					$data['html'] = $video['attributes']['share_embed'];
					$data['thumbnail_url'] = $video['attributes']['poster'];
					$data['author_name'] = $video['attributes']['nom_chaine'];
					$data['author_url'] = 'https://www.canal-u.tv' . $video['attributes']['lien_chaine'];
					$data['provider_name'] = 'Canal-U';
					$data['provider_url'] = 'https://www.canal-u.tv';
				}
			}
		}
	}
	
	return $data;
}
