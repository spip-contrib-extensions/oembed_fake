<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function oembed_fake_oembed_lister_providers($providers) {
	$providers['http://*.canal-u.tv/chaines/*/*'] = $GLOBALS['meta']['adresse_site'] . '/oembed_fake.api/canalu';
	
	return $providers;
}
