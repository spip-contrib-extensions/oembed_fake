<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/oembed.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'oembed_fake_description' => 'Ce plugin simule le fonctionnement oEmbed pour des services qui ne le fournissent pas eux-mêmes, en récupérant les bonnes informations et code d’insertion dans le JSON.',
	'oembed_fake_nom' => 'oEmbed : services sans oEmbed',
	'oembed_fake_slogan' => 'Permet d’utiliser oEmbed avec certains services qui ne le fournissent pas eux-mêmes.',
);
